# Hacknizer Api

## Setup

- To set up the API server, run:
  ```
  docker-compose build
  docker-compose run api rails db:setup
  docker-compose up
  ```
  **NOTE:** These commands will **create a Docker image for the API**,
  **set up the database with the latest schema** and **load the server**.
  By default, the API will be available at `localhost:3000`.

## Observations

- The `build` operation may take a while depending on your internet connetion.
  It will download all dependencies specified at `Gemfile` and `Gemfile.lock`
  whenever these files are changed.

- API calls are **versioned**. All calls must be prefixed with a version id.
  This way, a `GET` call to retrieve a list with all hackathons would be
  `/v1/hackathons` (check the current version of the API to see the correct
  prefix).

- Hacknizer API uses two databases:
  - [PostgreSQL](https://www.postgresql.org/), an open source relational
    database management system (RDBMS). It is used by Rails to persist objects
    of a given class as tuples in a corresponding table.
  - [Redis](https://redis.io/), an open source in-memory data structure store.
    It is used by Rails to create a cache to speed up HTTP responses.

- The application and the databases are configured with environment variables,
  accordingly to [The Twelve-Factor App](https://12factor.net/). Development
  settings are available in the file `.env`. **These settings must be overriden
  for non-development environments**.

# Development

## Scaffolds

- To generate a full CRUD using the Rails CLI, run:
  ```
  docker-compose run --user="$(id -u):$(id -g) api rails g scaffold NAME
  ```
  **NOTE:** Linux users **must** put the flag `--user` to create files owned
  by their users instead of root. This flag is not be necessary on Windows
  and Mac, which run Docker on a Virtual Machine.

- To see a list of all generators available, run:
  ```
  docker-compose run --no-deps api rails g --help
  ```

- To generate the latest schema from migrations, run:
  ```
  docker-compose run api rails db:migrate
  ```

## Tests

- To configure the test database, run:
  ```
  docker-compose run -e RAILS_ENV=test api rails db:setup
  ```

- To run unit tests made with RSpec, run:
  ```
  docker-compose run api rails spec
  ```
  **NOTE:** By default, RSpec already assumes test environment.

## Runtime

- To access a Rails console, **execute the API** and run:
  ```
  docker-compose exec api rails c
  ```

- To access a PostgreSQL interactive prompt, **execute the API** and run:
  ```
  docker-compose exec postgres psql -U hacknizer
  ```

## Known issues

- Compose emits a warning when starting the API after the Website:
  ```
  WARNING: Found orphan containers (hacknizer_web_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
  ```
  This is the expected behavior, given that containers in the same namespace
  (`hacknizer`) but not listed in `docker-compose.yml` may be orphans.

- Redis may emit two warnings when it is started:
  ```
  WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
  WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
  ```
  For the development environment, no action is needed. Both settings are
  system-wide configurations that cannot be enabled only for the Redis'
  container. For further info, check:
  - https://github.com/docker-library/redis/issues/14
  - https://github.com/docker-library/redis/issues/55
